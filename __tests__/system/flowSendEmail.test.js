import { By } from 'selenium-webdriver'
import { webDriver } from './helpers'
import { api } from '../../helpers'

describe('[SYSTEM] Email flow', () => {
  let driver

  beforeAll(async () => {
    driver = await webDriver()
  })

  afterAll(async function() {
    await driver.quit()
  })

  it('expect show history record after send email', async () => {
    const randomSubject = `test email: ${Math.random()
      .toString(36)
      .substring(7)}`

    await driver.findElement(By.name('from')).sendKeys('woraponok@gmail.com')
    await driver.findElement(By.name('to')).sendKeys('woraponok@gmail.com')
    await driver.findElement(By.name('subject')).sendKeys(randomSubject)
    await driver.findElement(By.name('html')).sendKeys('system test')
    await driver.findElement(By.xpath("//button[@type='submit']")).click()
    await driver.sleep(8000)
    await driver.get('http://localhost:3000/history')
    await driver.sleep(2000)

    const textInPage = await driver.findElement(By.tagName('body')).getText()
    const { data } = await api.getMailHistory()
    expect(textInPage.includes(data[0].subject)).toEqual(true)
  }, 20000)
})
