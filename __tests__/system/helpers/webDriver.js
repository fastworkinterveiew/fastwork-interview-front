import { Builder } from 'selenium-webdriver'

const webDriver = async () => {
  try {
    const driver = await new Builder().forBrowser('chrome').build()
    await driver.get('http://localhost:3000')
    return driver
  } catch (err) {
    console.log(err)
  }
}

export default webDriver
