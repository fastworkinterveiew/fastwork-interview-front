import React from 'react'
import renderer from 'react-test-renderer'
import { mount } from 'enzyme'

import SidebarLayout from '../../../../../components/commons/layout/sidebar'

describe('[INTEGRATION] Sidebar Layout Component', () => {
  describe('With Snapshot Testing', () => {
    it('expect Sidebar Layout is toMatchSnapshot', () => {
      const component = renderer.create(
        <SidebarLayout collapsed={false} menuKey="index" />
      )
      const tree = component.toJSON()
      expect(tree).toMatchSnapshot()
    })
  })

  describe('With Enzyme', () => {
    it('expect sidebar is minimize when click menu fold', () => {
      const app = mount(<SidebarLayout collapsed={true} menuKey="index" />)
      expect(
        app
          .find('.ant-menu-item-selected')
          .containsMatchingElement(<span>ส่งอีเมลล์</span>)
      ).toEqual(true)
      expect(app.find('.ant-layout-sider-collapsed')).toHaveLength(1)
    })
    it("expect Mainlayout is highlight in newmail menu when props.menuKey is ''", () => {
      const app = mount(<SidebarLayout collapsed={false} menuKey="index" />)
      expect(
        app
          .find('.ant-menu-item-selected')
          .containsMatchingElement(<span>ส่งอีเมลล์</span>)
      ).toEqual(true)
    })
    it("expect Mainlayout is highlight in history menu when props.menuKey is 'history'", () => {
      const app = mount(<SidebarLayout collapsed={false} menuKey="history" />)
      expect(
        app
          .find('.ant-menu-item-selected')
          .containsMatchingElement(<span>ประวัติการส่งเมลล์</span>)
      ).toEqual(true)
    })
  })
})
