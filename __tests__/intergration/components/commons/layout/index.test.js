import React from 'react'
import renderer from 'react-test-renderer'
import { mount } from 'enzyme'

import { Layout } from '../../../../../components/commons'

describe('[INTEGRATION] Layout Component', () => {
  describe('With Snapshot Testing', () => {
    it('expect Layout is toMatchSnapshot', () => {
      const component = renderer.create(
        <Layout router={{ pathname: '' }} title="title">
          เทส Layout
        </Layout>
      )
      const tree = component.toJSON()
      expect(tree).toMatchSnapshot()
    })
  })

  describe('With Enzyme', () => {
    it('expect sidebar is minimize when click menu fold', () => {
      const app = mount(
        <Layout router={{ pathname: '' }} title="title">
          เทส Layout
        </Layout>
      )
      app.find('.anticon-menu-fold').simulate('click')
      expect(app.find('.ant-layout-sider-collapsed')).toHaveLength(1)
    })

    it("expect sidebar is highlight in newmail menu when pathname is ''", () => {
      const app = mount(
        <Layout router={{ pathname: '' }} title="title">
          เทส Layout
        </Layout>
      )

      expect(
        app
          .find('.ant-menu-item-selected')
          .containsMatchingElement(<span>ส่งอีเมลล์</span>)
      ).toEqual(true)
    })

    it("expect sidebar is highlight in history menu when pathname is 'history'", () => {
      const app = mount(
        <Layout router={{ pathname: 'history' }} title="title">
          เทส Layout
        </Layout>
      )

      expect(
        app
          .find('.ant-menu-item-selected')
          .containsMatchingElement(<span>ประวัติการส่งเมลล์</span>)
      ).toEqual(true)
    })

    it("expect sidebar is highlight in index menu when pathname is 'history' return false", () => {
      const app = mount(
        <Layout router={{ pathname: 'index' }} title="title">
          เทส Layout
        </Layout>
      )

      expect(
        app
          .find('.ant-menu-item-selected')
          .containsMatchingElement(<span>ประวัติการส่งเมลล์</span>)
      ).toEqual(false)
    })

    it("expect sidebar is highlight in history menu when pathname is 'index' return false", () => {
      const app = mount(
        <Layout router={{ pathname: 'history' }} title="title">
          เทส Layout
        </Layout>
      )

      expect(
        app
          .find('.ant-menu-item-selected')
          .containsMatchingElement(<span>ส่งอีเมลล์</span>)
      ).toEqual(false)
    })
  })
})
