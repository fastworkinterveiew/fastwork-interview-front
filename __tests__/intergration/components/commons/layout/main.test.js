import React from 'react'
import renderer from 'react-test-renderer'
import { shallow } from 'enzyme'

import MainLayout from '../../../../../components/commons/layout/main'

describe('[INTEGRATION] Main Layout Component', () => {
  describe('With Snapshot Testing', () => {
    it('expect Main Layout is toMatchSnapshot', () => {
      const component = renderer.create(
        <MainLayout title="title">เทส Main Layout</MainLayout>
      )
      const tree = component.toJSON()
      expect(tree).toMatchSnapshot()
    })
  })

  describe('With Enzyme', () => {
    it('expect Mainlayout display title from props.title', () => {
      const app = shallow(<MainLayout title="title">เทส Main Layout</MainLayout>)
      expect(app.containsMatchingElement(<h3>title</h3>)).toEqual(true)
    })

    it('expect Mainlayout display children from children', () => {
      const app = shallow(
        <MainLayout title="title">
          <div>เทส Main Layout</div>
        </MainLayout>
      )
      expect(app.contains(<div>เทส Main Layout</div>)).toEqual(true)
    })
  })
})
