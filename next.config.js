const withLess = require('@zeit/next-less')
const withCSS = require('@zeit/next-css')
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin')
const OptimizeCssnanoPlugin = require('@intervolga/optimize-cssnano-plugin')

module.exports = withCSS(
  withLess({
    publicRuntimeConfig: {
      NODE_ENV: process.env.NODE_ENV,
      API_URL: process.env.API_URL
    },
    lessLoaderOptions: {
      javascriptEnabled: true
    },
    webpack(config, { buildId, dev, isServer, defaultLoaders }) {
      if (!dev) {
        config.plugins.push(
          new OptimizeCssnanoPlugin({
            sourceMap: false,
            cssnanoOptions: {
              preset: [
                'default',
                {
                  discardComments: {
                    removeAll: true
                  }
                }
              ]
            }
          })
        ),
          config.plugins.push(
            new SWPrecacheWebpackPlugin({
              verbose: true,
              minify: true,
              staticFileGlobsIgnorePatterns: [/\.next\//],
              runtimeCaching: [
                {
                  handler: 'networkFirst',
                  urlPattern: /^https?.*/
                }
              ]
            })
          )
      }
      return config
    }
  })
)
