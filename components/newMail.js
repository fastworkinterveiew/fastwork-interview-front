import { Layout, Row, Col, Form, Input, Divider, Button } from 'antd'
import styled from 'styled-components'

export default ({ getFieldDecorator, handleSubmit, handleReset, isLoading }) => {
  return (
    <ContentStyle>
      <Row>
        <Col>
          <Form onSubmit={handleSubmit}>
            <FormInput
              getFieldDecorator={getFieldDecorator}
              type="email"
              name="from"
              prefix="จาก"
              message="กรุณากรอกอีเมลล์"
            />

            <FormInput
              getFieldDecorator={getFieldDecorator}
              type="email"
              name="to"
              prefix="ถึง"
              message="กรุณากรอกอีเมลล์"
            />

            <FormInput
              getFieldDecorator={getFieldDecorator}
              type="string"
              name="subject"
              prefix="เรื่อง"
              message="กรุณากรอกเรื่อง"
            />

            <FromItemStyle>
              {getFieldDecorator('html', {
                rules: [
                  {
                    message: 'กรุณากรอกข้อความ',
                    required: true
                  }
                ]
              })(<InputTextAreaStyle rows={10} name="html" placeholder="ข้อความ" />)}
            </FromItemStyle>
            <FormItemButton>
              <Button
                size="large"
                type="primary"
                htmlType="submit"
                loading={isLoading}
                disabled={isLoading}>
                ส่ง
              </Button>
              <Button size="large" onClick={handleReset}>
                ละทิ้ง
              </Button>
            </FormItemButton>
          </Form>
        </Col>
      </Row>
    </ContentStyle>
  )
}

const FormInput = ({ getFieldDecorator, type, name, prefix, message }) => {
  return (
    <>
      <FromItemStyle>
        {getFieldDecorator(name, {
          rules: [
            {
              required: true,
              type,
              message
            }
          ]
        })(<InputStyle name={name} prefix={prefix} />)}
      </FromItemStyle>
      <DividerStyle />
    </>
  )
}

const ContentStyle = styled(Layout.Content)`
  margin: 24px 16px;
  padding: 24px;
  background: #fff;
  overflow-y: scroll;
  min-height: 280px;
`

const FromItemStyle = styled(Form.Item)`
  margin-bottom: 0;
`

const InputStyle = styled(Input)`
  input {
    &.ant-input {
      border: none;
      padding-left: 50px !important;

      &:focus {
        outline: none;
        box-shadow: none;
      }
    }
  }
`

const InputTextAreaStyle = styled(Input.TextArea)`
  margin-top: 20px;
  border-color: #e8e8e8;
  border-top: none;
  border-left: none;
  border-right: none;
  border-radius: 0;

  &:focus,
  &:hover {
    outline: none;
    box-shadow: none;
    border-color: #e8e8e8;
  }
`

const DividerStyle = styled(Divider)`
  margin: 0;
`

const FormItemButton = styled(Form.Item)`
  text-align: right;

  button {
    margin-top: 10px;
    margin-left: 10px;
    padding: 0 25px;
  }
`
