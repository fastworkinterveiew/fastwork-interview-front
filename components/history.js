import { Layout, Row, Col, Table, Tag, Icon } from 'antd'
import styled from 'styled-components'

import { Modal } from '../components/commons'

const filterProvider = (record, providerOptions) => {
  return (
    record.providers
      .filter(provider => {
        return providerOptions.success ? provider.success : !provider.success
      })
      .map(provider => provider.name)
      .indexOf(providerOptions.name) >= 0
  )
}

const tableColumns = [
  {
    title: 'หัวข้อ',
    dataIndex: 'subject',
    key: 'subject'
  },
  {
    title: 'ผู้ส่ง',
    dataIndex: 'from',
    key: 'from'
  },
  {
    title: 'ผู้รับ',
    dataIndex: 'to',
    key: 'to'
  },
  {
    title: 'ผู้ให้บริการ',
    dataIndex: 'providers',
    key: 'providers',
    filters: [
      {
        text: 'ส่งสำเร็จด้วย MailGun',
        value: 'mailGunSuccess'
      },
      {
        text: 'ส่งสำเร็จด้วย Gmail',
        value: 'gmailSuccess'
      },
      {
        text: 'ส่งไม่สำเร็จด้วย MailGun',
        value: 'mailGunFailure'
      },
      {
        text: 'ส่งไม่สำเร็จ',
        value: 'allFailure'
      }
    ],
    onFilter: (value, record) => {
      let recordFilter = []

      switch (value) {
        case 'mailGunSuccess':
          recordFilter = filterProvider(record, { name: 'mailGun', success: true })
          break
        case 'gmailSuccess':
          recordFilter = filterProvider(record, { name: 'gmail', success: true })
          break
        case 'mailGunFailure':
          recordFilter = filterProvider(record, { name: 'mailGun', success: false })
          break
        case 'allFailure':
          recordFilter = filterProvider(record, { name: 'gmail', success: false })
          break
      }
      return recordFilter
    },
    render: providers => {
      return (
        <>
          {providers.map(provider => (
            <Tag key={provider._id} color={provider.success ? 'green' : 'red'}>
              {provider.name}
            </Tag>
          ))}
        </>
      )
    }
  },
  {
    title: 'วันที่',
    dataIndex: 'publishDate',
    key: 'publishDate',
    defaultSortOrder: 'descend',
    sortDirections: ['descend'],
    sorter: (a, b) => {
      return new Date(a.publishDate) - new Date(b.publishDate)
    },
    render: date => {
      const today = new Date()
      const publishDateString = new Date(date)
      const isToday = today.toDateString() === publishDateString.toDateString()
      return (
        <>
          {isToday
            ? publishDateString.toLocaleTimeString()
            : publishDateString.toDateString()}
        </>
      )
    }
  },
  {
    title: 'รายละเอียด',
    key: 'operation',
    align: 'center',
    render: data => {
      return <Icon type="eye" onClick={Modal.mailInfo(data)} />
    }
  }
]

export default ({ mailsHistory }) => {
  const data = mailsHistory.map((mail, index) => {
    return {
      ...mail,
      key: index
    }
  })

  return (
    <ContentStyle>
      <Row>
        <Col>
          <Table
            pagination={{ simple: true, defaultCurrent: 1, pageSize: 8 }}
            columns={tableColumns}
            dataSource={data}
          />
        </Col>
      </Row>
    </ContentStyle>
  )
}

const ContentStyle = styled(Layout.Content)`
  margin: 24px 16px;
  padding: 24px;
  background: #fff;
  overflow-y: scroll;
  min-height: 280px;
`
