import React, { PureComponent } from 'react'
import { Layout } from 'antd'
import styled from 'styled-components'
import { withRouter } from 'next/router'

import Sidebar from './sidebar'
import Main from './main'

class LayoutCommon extends PureComponent {
  state = {
    collapsed: false
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    })
  }

  handleRoute = ({ key }) => {
    this.props.router.push({
      pathname: `/${key}`
    })
  }

  getMenuKey = () => {
    const { router } = this.props
    const menuKey = router.pathname.replace(/^\//, '') || 'index'

    return menuKey
  }

  render() {
    const { children, title } = this.props
    const { collapsed } = this.state

    return (
      <LayoutStyle>
        <Sidebar
          handleRoute={this.handleRoute}
          menuKey={this.getMenuKey()}
          collapsed={collapsed}
        />
        <Main collapsed={collapsed} toggle={this.toggle} title={title}>
          {children}
        </Main>
      </LayoutStyle>
    )
  }
}

export default withRouter(LayoutCommon)

const LayoutStyle = styled(Layout)`
  position: relative;
  overflow-x: hidden;
  height: 100vh;
`
