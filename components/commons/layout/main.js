import { Layout, Icon, Row, Dropdown } from 'antd'
import styled from 'styled-components'

const MainLayout = ({ collapsed, toggle, title, children }) => {
  return (
    <LayoutStyle collapsed={collapsed ? 'collapsed' : 'un-collapsed'}>
      <HeaderStyle>
        <Row type="flex">
          <Icon
            className="trigger"
            type={collapsed ? 'menu-unfold' : 'menu-fold'}
            onClick={toggle}
          />
          <h3>{title}</h3>
        </Row>
      </HeaderStyle>
      {children}
    </LayoutStyle>
  )
}

export default MainLayout

const LayoutStyle = styled(Layout)`
  margin-left: ${props => (props.collapsed === 'collapsed' ? '80px' : '200px')};

  .trigger {
    font-size: 18px;
    line-height: 64px;
    padding: 0 24px;
    cursor: pointer;
    transition: color 0.3s;
  }
  .trigger:hover {
    color: #1890ff;
  }
`

const HeaderStyle = styled(Layout.Header)`
  background: #fff;
  padding: 0;
`

const DropdownStyle = styled(Dropdown)`
  display: flex;
  margin-left: auto;
  margin-top: auto;
  margin-bottom: auto;
  height: 100%;
`

const LinkStyle = styled.div`
  height: 100%;
  display: inline-block;
  padding: 0 24px;
  cursor: pointer;
`
