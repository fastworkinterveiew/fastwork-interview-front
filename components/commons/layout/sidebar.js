import { Layout, Menu, Icon } from 'antd'
import styled from 'styled-components'

const Sidebar = ({ collapsed, handleRoute, menuKey }) => {
  return (
    <LayoutSiderStyle trigger={null} collapsible collapsed={collapsed}>
      <img className="logo" src="static/img/logo.png" alt="fastwork_logo" />
      <Menu
        defaultSelectedKeys={['index']}
        selectedKeys={[menuKey]}
        defaultOpenKeys={['sub1']}
        mode="inline"
        theme="dark"
        inlineCollapsed={collapsed}>
        <Menu.Item key="index" onClick={handleRoute}>
          <Icon type="mail" />
          <span>ส่งอีเมลล์</span>
        </Menu.Item>
        <Menu.Item key="history" onClick={handleRoute}>
          <Icon type="line-chart" />
          <span>ประวัติการส่งเมลล์</span>
        </Menu.Item>
      </Menu>
    </LayoutSiderStyle>
  )
}

export default Sidebar

const LayoutSiderStyle = styled(Layout.Sider)`
  overflow: auto;
  height: 100vh;
  position: fixed;
  left: 0;

  .logo {
    background: white;
    padding: 8px;
    margin: 16px 10px;
    width: 90%;
  }
`
