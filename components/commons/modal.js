import { Row, Col, Modal, Card, Divider } from 'antd'
import styled from 'styled-components'

export default {
  mailInfo: props => () => {
    Modal.info({
      title: 'รายละเอียด',
      okText: 'ปิด',
      content: <MailInfo {...props} />
    })
  }
}

const MailInfo = ({ from, to, html }) => {
  return (
    <CardStyle>
      <Row>
        <Col>
          <b>จาก:</b> {from}
        </Col>
      </Row>
      <Row>
        <Col>
          <b>ถึง:</b> {to}
        </Col>
        <Divider />
      </Row>
      <Row>
        <Col>
          <b>ข้อความ:</b> {html}
        </Col>
      </Row>
    </CardStyle>
  )
}

const CardStyle = styled(Card)`
  margin-left: -38px;
`
