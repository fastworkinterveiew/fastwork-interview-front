# Fastwork Interview Front-End

Fastwork Interview Front-end using Next.js, Node.js, Express

---

## 1.Run Dev

```sh
$ npm install
$ npm run dev
```

## 2.Run Production

```sh
$ npm install
$ npm run build
$ npm run start
```

## 3.Run Test

รัน step 1 หรือ 2 ก่อนเพื่อให้สามารถ run system test (selenium) ได้

```sh
$ open new terminal and cd <to project>
$ npm run test
```

---

# อธิบายเพิ่มเติม

---

## File Structor

/pages -> statefull
/components -> stateless

## เทคโนลียีที่ใช้

1. next.js + ant design + PWA(progressive web application: manifest.json + service worker)
2. express: serve webapplication ด้วย gzip และทำ cache ให้รูปภาพ (custom server)
3. jest + enzyme + selenium (สำหรับเทส): **ปัญหาที่พบคือ** jest ไม่สามารถอ่านตัวแปรจาก env ที่เรา config ไว้ได้ **แก้ปัญหาโดย**ไปกำหนดตัวแปรเพิ่มที่ jest.config.js

## ประกอบด้วย 2 หน้า

/index: เป็นหน้าที่เอาไว้ส่ง mail

/history: ดู record ของการส่งเมลล์ทั้งหมดโดยที่หน้า history สามารถกด filter เลือก email provider ได้ตรงปุ่ม filter หัวตาราง และกดดูรายละเอียดของ email นั้นๆได้

![filter search](http://i.imgur.com/8Vye2FD.png)

## deploy

digital ocean: เลือกใช้เพราะราคาจับต้องได้, เป็น ssd,มี community พอสมควร และสามารถใช้งานได้ง่าย

## optimize

1. serve webapplication ด้วย gzip ทำให้ขนาดไฟล์ js ต่างๆถูก compression ทำให้ไฟล์ต่างๆเล็กลง client โหลด web ได้ไว้ขึ้น
2. ทำให้ไฟล์ css (antdesign) เล็กลงด้วยการทำให้เป็น minify ด้วย OptimizeCssnanoPlugin
3. กำหนดให้ รูปภาพที่มาจาก /static ถูกเก็บไว้ใน cache
4. ใช้ react.PureComponent แทน react.Component เพื่อลดการ rerender ของ compoent ที่ไม่จำเป็น

ผล gzip
![filter search](http://i.imgur.com/FhpzrIo.png?1)

ผล css minify
![filter search](http://i.imgur.com/FkzsIuA.png)

## pwa

ปัญหาที่พบคือบน digital ocean ยังไม่สามารถรัน service worker ของ pwa ได้
ในตอนนี้สามารถเทส PWA ได้บน localhost โดยต้องรันในโหมด Production ตาม step ที่ 2 จะสามารถทดสอบ PWA ได้ (สามารถใช้งานแบบ offline ได้ ทดสอบได้จาก menu audit บน developer tool ของ googlechrome)

![filter search](http://i.imgur.com/U7fp0M5r.png)

## อื่นๆ

\*\* สิ่งที่ควรทำเพิ่มเติมคือปรับให้เป็น https,http/2 เพื่อเพิ่มความปลอดภัยและยังเพิ่มคะแนน ligthouse ได้อีกพอสมควรซึ่งส่งผลให้มีคะแนน seo ที่ดีขึ้น
