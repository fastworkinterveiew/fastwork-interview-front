import axios from 'axios'
import getConfig from 'next/config'

const {
  publicRuntimeConfig: { API_URL }
} = getConfig()
const instance = axios.create({
  baseURL: API_URL,
  timeout: 10000
})

export const api = {
  sendMail: data => instance.post('/api/v1/mail/send', data),
  getMailHistory: () => instance.get('/api/v1/mail/history')
}
