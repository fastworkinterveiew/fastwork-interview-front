import Document, { Head, Main, NextScript } from 'next/document'
import { ServerStyleSheet } from 'styled-components'

export default class MyDocument extends Document {
  static getInitialProps({ renderPage }) {
    const sheet = new ServerStyleSheet()
    const page = renderPage(App => props => sheet.collectStyles(<App {...props} />))
    const styleTags = sheet.getStyleElement()
    return { ...page, styleTags }
  }

  render() {
    return (
      <html lang="th">
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
          <meta name="description" content="Fastwork Interview Email Sender" />
          {this.props.styleTags}

          {/* pwa support */}
          <meta name="mobile-web-app-capable" content="yes" />
          <meta name="apple-mobile-web-app-capable" content="yes" />
          <meta name="application-name" content="Fastwork Interview" />
          <meta name="apple-mobile-web-app-title" content="Fastwork Interview" />
          <meta name="theme-color" content="#000" />
          <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
          <meta name="msapplication-starturl" content="/" />

          <link rel="manifest" href="/manifest.json" />
          <link rel="icon" href="static/img/icon.png" />
          <link rel="apple-touch-icon" href="static/img/icon.png" />
          {/* end pwa support */}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}
