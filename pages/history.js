import React, { PureComponent } from 'react'

import { Layout } from '../components/commons/'
import { History } from '../components'
import { api } from '../helpers'

class HistoryPage extends PureComponent {
  static async getInitialProps() {
    const { data } = await api.getMailHistory()
    return { mailsHistory: data }
  }

  render() {
    const { mailsHistory } = this.props

    return (
      <Layout title="ดูประวัติการส่งเมลล์ย้อนหลัง">
        <History mailsHistory={mailsHistory} />
      </Layout>
    )
  }
}

export default HistoryPage
