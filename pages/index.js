import React, { PureComponent } from 'react'
import { Form } from 'antd'
import { compose } from 'recompose'
import { message } from 'antd'

import { Layout } from '../components/commons/'
import { NewMail } from '../components'
import { api } from '../helpers'

class NewMailPage extends PureComponent {
  state = {
    sendMailStatus: {
      isLoading: false
    }
  }

  handleSubmit = e => {
    e.preventDefault()
    const { form } = this.props

    form.validateFields(async (err, values) => {
      if (!err) await this.sendEmail(values)
    })
  }

  handleReset = () => {
    const { form } = this.props
    form.resetFields()
  }

  sendEmail = async values => {
    try {
      this.setState({ sendMailStatus: { isLoading: true } })
      await api.sendMail(values)
      message.success('ส่งอีเมลล์สำเร็จ')
    } catch (err) {
      message.error('ส่งอีเมลล์ไม่สำเร็จ กรุณาลองใหม่ภายหลัง')
    }
    this.handleReset()
    this.setState({ sendMailStatus: { isLoading: false } })
  }

  render() {
    const { sendMailStatus } = this.state
    const {
      form: { getFieldDecorator }
    } = this.props

    return (
      <Layout title="ข้อความใหม่">
        <NewMail
          getFieldDecorator={getFieldDecorator}
          handleSubmit={this.handleSubmit}
          handleReset={this.handleReset}
          isLoading={sendMailStatus.isLoading}
        />
      </Layout>
    )
  }
}

const enhance = compose(Form.create({ name: 'sendMailForm' }))

export default enhance(NewMailPage)
