import React from 'react'
import App, { Container } from 'next/app'
import getConfig from 'next/config'
import Head from 'next/head'
import 'antd/dist/antd.less'

class MyApp extends App {
  static async getInitialProps({ Component, router, ctx }) {
    let pageProps = {}

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }
    return { pageProps }
  }

  componentDidMount() {
    const {
      publicRuntimeConfig: { NODE_ENV }
    } = getConfig()

    if ('serviceWorker' in navigator && NODE_ENV === 'production') {
      navigator.serviceWorker
        .register('/service-worker.js')
        .then(registration => {
          console.log('service worker registration successful')
        })
        .catch(err => {
          console.log('service worker registration failed', err.message)
        })
    }
  }

  render() {
    const { Component, pageProps } = this.props

    return (
      <Container>
        <Head>
          <title>Fastwork Interview</title>
        </Head>
        <Component {...pageProps} />
      </Container>
    )
  }
}

export default MyApp
