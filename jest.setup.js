import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { setConfig } from 'next/config'

setConfig({
  publicRuntimeConfig: { NODE_ENV: 'test', API_URL: 'http://104.248.151.6:8000' },
  lessLoaderOptions: { javascriptEnabled: true }
})
configure({ adapter: new Adapter() })
